import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, SafeAreaView, Image, ScrollView, ActivityIndicator } from 'react-native'
import { Colors } from '../utils/Colors'
import { getNext, PokemonType } from '../redux/pokedexSlice'
import Axios from 'axios'

import PokemonInfo from './containers/PokemonInfo'


const capitalize = (value: string) => {
    return value.charAt(0).toUpperCase() + value.slice(1)
}

const getIdFromUrl = (urlFull: string) => {

    let cleanUrl = urlFull.substring(0, urlFull.length - 1);
    let url = urlFull;
    url = url.substring(0, url.length - 2)
    let lastSlash = url.lastIndexOf("/")
    return cleanUrl.substring(lastSlash + 1)
}



const Pokemon = ({ route, navigation }) => {

    const [Details, setDetails] = useState({})
    const [Species, setSpecies] = useState({})
    const { pokemon }: { pokemon: PokemonType } = route.params


    useEffect(() => {

        navigation.setOptions({
            title: capitalize(pokemon.name),
        });
    }, [])

    const getDetails = () => {
        console.log("getDetails", pokemon.url)
        Axios.get(pokemon.url)
            .then(({ data }) => {
                setDetails(data)
            }).
            catch((e) => {
                console.warn("getDetails", e)
                alert("Pokemon not found")
            })
    }
    const getSpecies = () => {
        Axios.get(`https://pokeapi.co/api/v2/pokemon-species/${getIdFromUrl(pokemon.url)}`)
            .then(({ data }) => {
                // console.log("getSpecies respo", data)
                setSpecies(data)
            }).
            catch((e) => {
                console.log("getSpecies", e)
                alert("Pokemon not found")
            })
    }

    useEffect(() => {
        getDetails()
        getSpecies()
    }, [])

    return (
        <View style={styles.container}>
            <SafeAreaView style={styles.safeAreaStyle}>
                {Object.keys(Details).length > 0 ? <PokemonInfo {...{ Details, Species, pokemon }} /> : <ActivityIndicator />}
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%"
    },
    safeAreaStyle: {
        backgroundColor: Colors.gray,
        flex: 1,

    }

})

export default Pokemon
