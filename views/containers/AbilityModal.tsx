import React, {useState} from 'react'
import { Alert, Modal, StyleSheet, Text, Pressable, View, TouchableOpacity } from "react-native";
import { Colors } from '../../utils/Colors';
import Axios from 'axios'

const AbilityModal = ({abilities} : {abilities : []}) => {
    const [Data, setData] = useState(null)
    const [Selected, setSelected] = useState({})

    const getAbilityDetails = (url:string) => {
        console.log("getAbilityDetails", url)
        Axios.get(url)
        .then(({data}) => {
            console.log("abilities resp", data)
            setData(data)
        }).catch((e) => {
            alert("No se pudo obtener las abilidades")
        })
    }

    const findDescriptionForEnglish = () => {
       let details = Data.effect_entries.find((entry) => {         
            return entry.language.name == "en" 
        })

        return <Text key={1} style={styles.modalText}>{details.effect}</Text>
    }

    const AbilityRow = (row) => {
        return <View style={styles.abilityRow} >
                <Text style={styles.abilityName}>{"-"} {row.ability.name}</Text>
                <TouchableOpacity onPress={() => {
                    setSelected(row.ability)
                    getAbilityDetails(row.ability.url)
                }} style={styles.detailBtn}>
                    <Text style={{
                        color : "#fff"
                    }}>Detalles</Text>
                </TouchableOpacity>
        </View> 
    }

    return (
        <View>
            <Text style={styles.subtitle}>Habilidades</Text>
            <View style={{
                
            }}>
                {abilities.map((row, key) => {
                  return <AbilityRow key={key} {...row}/> 
                })}
            </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={Data != null}
                style={{
                    position : 'absolute',
                    width : "100%"
                }}
                onRequestClose={() => setData(null)}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                    <Text style={styles.modalTitle}>{Selected ? Selected.name : ""}</Text>
                        {Data != null && [findDescriptionForEnglish(),
                        <Pressable
                            key={2}
                            style={[styles.buttonClose]}
                            onPress={() => {
                                setData(null)
                            }}
                        >
                            <Text style={styles.textStyle}>Cerrar</Text>
                        </Pressable>]}
                    </View>
                </View>
            </Modal>
        </View>
        
    )
}

const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    //   marginTop: 22
    },
    subtitle : {
        fontSize : 30
    },
    modalView: {
      margin: 20,
      backgroundColor: Colors.red,
      borderRadius: 20,
      padding: 35,
      paddingTop :20,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 2,
        height: 8
      },
      shadowOpacity: 0.45,
      shadowRadius: 8,
      elevation: 8
    },
    buttonClose: {
      backgroundColor: "#fff",
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    textStyle: {
      color: Colors.red,
      padding : 5,
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 35,
      marginTop: 25,
      fontSize : 20,
      color : "#fff",
      textAlign: "center"
    },
    modalTitle : {
        fontSize : 30,
        fontWeight : "600",
        color : "#fff"
    },
    abilityRow : {
        padding : 10,
        borderBottomColor : "#333",
        borderBottomWidth : 1,
        alignItems : 'center',
        flexDirection : 'row',
        justifyContent : 'space-between'
    },
    detailBtn : {
        height : 30,
        width : 100,
        backgroundColor : Colors.red,
        padding : 5,
        justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 5
    },
    abilityName : {
        fontSize : 16,
        fontWeight : "800"
    }
  });
  

export default AbilityModal
