import React from 'react'
import { View, Text, Image, ScrollView, StyleSheet } from 'react-native'
import AbilityModal from './AbilityModal'
import MovesModal from './MovesModal'
import { PokemonType } from '../../redux/pokedexSlice'
import Evolutions from './Evolutions'

const PokemonInfo = ({Details, Species, pokemon} : {Details : any, Species:any, pokemon : PokemonType}) => {


    return <View style={styles.container}>
            <View style={styles.headerBg}>
                    
                    <Image style={{
                        width : 100,
                        height : 100
                    }} source={{
                        uri : Details.sprites.other["official-artwork"].front_default
                    }}></Image>

                    <View style={{
                        flex : 1,
                        height : "100%",
                        padding : 10
                    }}>
                        <Text style={styles.detailTitle}>
                            Altura:
                            <Text style={styles.detailInfo}>
                                {Details.height}
                            </Text>
                        </Text>
                        <Text style={styles.detailTitle}>
                            Peso:
                            <Text style={styles.detailInfo}>
                                {Details.weight}
                            </Text>
                        </Text>
                        <Text style={styles.detailTitle}>
                            Exp. Base:
                            <Text style={styles.detailInfo}>
                                {Details.base_experience}
                            </Text>
                        </Text>
                        <Text style={styles.detailTitle}>
                            Tipo:
                            <Text style={styles.detailInfo}>
                                {Details.types.map((row : {type : {name : string; url : string}}) => row.type.name).join(", ")}
                            </Text>
                        </Text>
                    
                    </View>
                    
            </View>
            <Evolutions chain={Species.evolution_chain} />
            <ScrollView style={styles.scrollViewStyle}>
                <AbilityModal abilities={Details.abilities} />
                <MovesModal moves={Details.moves}/>
            </ScrollView>
    </View>
}

const styles = StyleSheet.create({
    headerBg : {
        width : "80%",
        height : 120,
        marginLeft : "10%",
        marginRight : "10%",
        alignItems : "center",
        backgroundColor : '#fff',
        flexDirection : 'row',
        borderRadius : 10
    },
    container : {
        flex : 1,
        paddingTop : 20
    },
    scrollViewStyle : {
        flex :1,
        borderRadius : 5,
        padding : 10,
        backgroundColor : "#fff",
        margin : 20
    },
    detailTitle : {
        textAlign : 'left',
        fontSize : 16,
        fontWeight : "700"
    },
    detailInfo : {
        textAlign : 'left',
        fontSize : 16,
        fontWeight : "300"
    }
   
})

export default PokemonInfo
