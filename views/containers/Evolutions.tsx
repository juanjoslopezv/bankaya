import React, { useEffect, useState } from 'react'

import { View, Text, Image, ScrollView, StyleSheet } from 'react-native'
import Axios from 'axios'

let evolutionChain :any = []

const getEvolutionId = evo => {

    let cleanUrl = evo.url.substring(0, evo.url.length - 1);
    let url = evo.url;
    url = url.substring(0, url.length - 2)
    let lastSlash = url.lastIndexOf("/")
    return cleanUrl.substring(lastSlash + 1)
}

const Evolutions = ({chain} : {chain :any}) => {

    const [EvolutionChain, setEvolutionChain] = useState(null)

    useEffect(() => {
        console.log("getChaing", chain)
        if(chain)
            Axios.get(`${chain.url}`)
            .then(({data}) => {
                setEvolutionChain(data)
            })
        
    }, [chain])

    evolutionChain = []

    const navigateEvolutionTree = (node:any) => {
        
        let evolutionInfo = Object.assign({}, node.species, {details : node.evolution_details[0]})
        evolutionChain.push(evolutionInfo)
        if(node.evolves_to.length > 0)
            navigateEvolutionTree(node.evolves_to[0])
    }

    EvolutionChain && navigateEvolutionTree(EvolutionChain.chain)

    return (
        <View style={styles.container}>
        <ScrollView horizontal={true} style={styles.scrollContainer}>
            <View style={styles.interiorContainer}>
                {evolutionChain.map((evo, key) => {
                    let evolutionId = getEvolutionId(evo)
                    return <View key={key} style={styles.evolutionContainer}>
                        <Text style={styles.minLvl}>{evo.details?.min_level}</Text>
                        <Image style={styles.pokemonImg} source={{
                            uri : `https://pokeres.bastionbot.org/images/pokemon/${evolutionId}.png`
                        }} />
                        <Text style={{
                            fontSize : 20
                        }}>{evo.name}</Text>
                        
                        
                    </View>
                })}
            </View>
        </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container : {
        height : 130,
        width : "90%",
        marginLeft : "5%",
        marginRight : "5%",
        justifyContent : 'center',
        alignItems : 'center'
    },
    scrollContainer : {
        height : "100%",
        width : "100%",
        marginTop  :20,
        flexDirection : 'row',
        backgroundColor : "#fff"
    },
    interiorContainer : {
        flex : 1,
        flexDirection : 'row',
        justifyContent : 'center',
        alignItems : 'center'
    },
    evolutionContainer : {
        height : "100%",
        width : 135,
        borderEndWidth : 1,
        borderEndColor : "#333",
        //marginLeft : 10,
        position: 'relative',
        padding : 10,
        justifyContent : 'space-between',
        alignItems : 'center'
    },
    minLvl : {
        position : 'absolute',
        top : 5,
        left : 5,
        fontSize : 20
    },
    pokemonImg : {
        height : 70,
        width : 70
    }
})

export default Evolutions
