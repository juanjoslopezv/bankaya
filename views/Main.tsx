import { useNavigation } from '@react-navigation/native'
import React, {useEffect, useState} from 'react'
import { View, Text, StyleSheet, FlatList, TouchableOpacity, SafeAreaView, ActivityIndicator } from 'react-native'
import { Colors } from '../utils/Colors'

import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../configureStore'
import { getAll, getNext, PokemonType } from '../redux/pokedexSlice'



const Main = () => {
    const pokedex = useSelector((state: RootState) => state.pokedex)
    const dispatch = useDispatch()
    const navigation = useNavigation()


    const getAllPokemons = () => {
        dispatch(getAll())
    }

    useEffect(() => {
        getAllPokemons();
        
    }, [])


    const goToPokemon = (pokemon : PokemonType, index : number) => {
        navigation.navigate("Pokemon", {
            pokemon,
            ID : index + 1
        })
    }

    const fetchNext = () => {
        console.log("fetchNext")
        dispatch(getNext(pokedex.data.next))
    }

    const renderPokemon = ({item, index} : {item : PokemonType, index:number}) => {
        return <TouchableOpacity onPress={() => goToPokemon(item, index)} style={styles.pokemonRow}>
            <Text style={styles.pokemonText}>{item.name}</Text>
            <Text style={styles.pokemonText}>{">"}</Text>
        </TouchableOpacity>
    }
    return (
        <View style={styles.container}>
            
            <SafeAreaView style={{
                backgroundColor : Colors.red,
                flex : 1
            }}>
                <View style={{
                    flexDirection : 'row',
                    justifyContent : 'center'
                }}>
                <Text style={styles.title}>PokePedia</Text>
                    {pokedex.fetching && <ActivityIndicator color="#fff" />}
                </View>
                
                <FlatList
                    data={pokedex.data.results}
                    contentContainerStyle={styles.pokemonCotainerList}
                    style={styles.pokemonList}
                    renderItem={renderPokemon}
                    refreshing={pokedex.fetching}
                    keyExtractor={(item) => item.name}
                    onEndReached={fetchNext}
                />
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width : "100%",
      backgroundColor: '#fff'
    },
    title : {
        fontSize : 50,
        fontWeight : "500",
        alignSelf : 'center',
        color : "#fff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        marginTop : 20
    },
    pokemonList : {
        margin : 20,
        marginLeft : 40,
        marginRight : 40,
        borderWidth : 20,
        borderRadius : 5,
        // height : "80%",
        borderColor : Colors.gray
    },
    pokemonCotainerList : {
        paddingBottom : 40,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    pokemonRow : {
        height : 80,
        width : "100%",
        backgroundColor : "#fff",
        borderBottomColor : "#333",
        borderBottomWidth : 1,
        alignItems : 'center',
        justifyContent : 'space-between',
        padding : 10,
        flexDirection : 'row'
    },
    pokemonText : {
        fontSize : 20,
        fontWeight : "700"
    }
  });

export default Main
