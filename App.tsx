import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Provider } from 'react-redux'
import store from './configureStore'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native';
import Main from './views/Main'
import Pokemon from './views/Pokemon';
import { Colors } from './utils/Colors';


const Stack = createStackNavigator()

export default function App() {
  return (
    <Provider store={store}>
       <NavigationContainer>
          <Stack.Navigator initialRouteName="Main">
              <Stack.Screen name="Main" component={Main} options={{
                headerShown: false
              }} />
              <Stack.Screen name="Pokemon" component={Pokemon} options={{
                // headerShown: false
                headerStyle : {
                  backgroundColor : Colors.red,
                  
                },
                headerBackTitleStyle : {
                  color : "#fff"
                },
                headerTintColor:"#fff"
              }} />
          </Stack.Navigator>
       </NavigationContainer>
    
      </Provider>
    
  );
}


