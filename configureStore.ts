import { createSlice, configureStore } from '@reduxjs/toolkit'
import pokedexSlice from './redux/pokedexSlice'
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  pokedex: pokedexSlice.reducer
});

export type RootState = ReturnType<typeof rootReducer>;

const store = configureStore({
  reducer: rootReducer
})

export default store