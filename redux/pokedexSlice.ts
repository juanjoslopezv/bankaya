import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'

import Axios, { AxiosInstance } from 'axios'

const api = Axios.create()

export const getNext = createAsyncThunk(
    'pokemon/getNext',
    async (url:string) => {
        const response = await api.get(
            url
        );
        return response.data;
    }
);
export const getAll = createAsyncThunk(
    'pokemon/getAll',
    async () => {
        const response = await api.get('https://pokeapi.co/api/v2/pokemon/');
        return response.data;
    }
);



export type PokemonType = {
    name:string;
    url:string;
}


const pokedexSlice = createSlice({
    name: 'pokedex',
    initialState : {
        fetching : false,
        data : {
            results : [],
            next : null,
            previous : null
        }
    },
    reducers: {
        
    },
    extraReducers: (builder) => {
        //GetAll
        builder.addCase(getAll.pending, (state) => {
            state.fetching = true;
        });
        builder.addCase(getAll.fulfilled, (state, action) => {
            state.fetching = false;
            state.data = action.payload

        });
        builder.addCase(getAll.rejected, (state, action) => {
            state.fetching = false;

        });
        //GetNext
        builder.addCase(getNext.pending, (state) => {
            state.fetching = true;
        });
        builder.addCase(getNext.fulfilled, (state, action) => {
            state.fetching = false;
            state.data.next = action.payload.next
            state.data.previous = action.payload.previous
            state.data.results = state.data.results.concat(action.payload.results)

        });
        builder.addCase(getNext.rejected, (state, action) => {
            state.fetching = false;

        });
       
    }
})

export default pokedexSlice